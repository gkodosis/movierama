resource "aws_elasticache_cluster" "movierama-cluster-tf" {
  cluster_id           = "movierama-cluster-tf"
  engine               = "Redis"
  node_type            = "cache.t2.small"
  port                 = "6379"
  num_cache_nodes      = "1"
  parameter_group_name = "default.redis6.x"
}
