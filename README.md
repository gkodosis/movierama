# MovieRama | Workable Coding Challenge

###### *This repository depicts a coding challenge of Workable assigned to me, aiming to discover my ability on creating infrastructures using automation tools and building web applications, answering to specific endpoints*

## *Task 1*

#### Environment Selection

Being familiar with AWS Cloud Services, I decided to use AWS in order to host, build and deploy my applications. 

* Initialized a free-tier account & granted admin privileges to my user
* Created an EC2 Ubuntu Server instance in order to build the project
* Used ElasticCache Redis service in order to create and specify the resources of my `movierama-cluster`

#### Automation Process

After creating my first redis cluster, adjusting to the task's requirements, I automated the process using Terraform.

* Downloaded and configured on my ubuntu-based machine
* Generated access keys for my aws-user profile and mapped them to terraform 
* Created a simple configuration file, specifying the aws resources used in `main.tf`
* Granted ***AdministratorAccess*** to enable the cluster creation
* Run the following commands to create my `movierama-cluster-tf` via Terraform

```
terraform init
terraform apply
```

#### Snapshots


![redis cluster](snapshots/redis_cluster.png)


###### *This snapshot depicts the creation of the two redis clusters and the details of the terraform-created one in AWS*


## *Task 2*

#### Implementation Process

For the purposes of this task, I experimented myself with a big variety of new technologies for me. Trying to map the endpoints with specific url paths consists a pretty complex task in the Kubernetes scene, especially on EKS-AWS.

##### AWS - Elastic Kubernetes Service (EKS)

> Manual Configuration: At first, I tried to create the appropriate infrastructure using the AWS user interface

* Created an IAM Role, attaching AmazonEKSClusterPolicy & AmazonEKSServicePolicy
* Created Cloud Formation stack, attaching the appropriate templates
* Initialized my EKS cluster, attaching my IAM role, configuring the already created vpc and accompanied subnets-security groups
* Installed `kubectl` and `aws-iam-authenticator` in order to run aws and kube commands
* Run the following commands to track my cluster

```
aws configure
aws eks --region us-east-2 update-kubeconfig --name nginxcluster
```

* Created another Cloud Formation stack configuring my nodes
* Prepared 3 yaml files in order to create the appropriate resources, using nginx as a web server: `aws-auth-cm.yaml`, `nginx.yaml` and `nginx-svc.yaml`, provides in the repository
* Run the following commands to apply the services to my cluster and track their creation process

```
kubectl apply -f aws-auth-cm.yaml
kubectl get nodes
kubectl apply -f nginx.yaml
kubectl apply -f nginx-svc.yaml
kubectl get svc
```

* Access the server via the [external-ip](http://ade80092-2048game-2048ingr-6fa0-192123492.us-east-2.elb.amazonaws.com/) of the server

> Problem Issued: Configuring custom paths for a LoadBalancer in an AWS supported cluster is a nightmare

##### Minikube

> Emerging towards Ingress Nginx Controller as the solution to enable routing functionality, I decided to use Minikube

* Installed docker and minikube on my local machine
* Run the following commands to initialize my kubernetes cluster and configure ingress

```
minikube start
minikube addons enable ingress
kubectl get pods -n kube-system
```

> Providing with mainly local capabilities, I left this solution for AWS again

##### AWS - Elastic Container Service (EKS) - ALBIngressController

> Understanding the manual complexities on AWS, I decided to retry this solution with eksctl

* Re-created an eks cluster via eksctl, following the commands listed above

```
kubectl get sa -n kube-system
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/master/docs/examples/rbac-role.yaml
eksctl create iamserviceaccount \
    --region region-code \
    --name alb-ingress-controller \  #Note:  K8S Service Account Name that need to be bound to newly created IAM Role
    --namespace kube-system \
    --cluster prod \
    --attach-policy-arn arn:aws:iam::111122223333:policy/ALBIngressControllerIAMPolicy \
    --override-existing-serviceaccounts \
    --approve
eksctl  get iamserviceaccount --cluster eksdemo1
kubectl describe sa alb-ingress-controller -n kube-system
# Deploy ALB Ingress Controller
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/master/docs/examples/alb-ingress-controller.yaml
# Verify Deployment
kubectl get deploy -n kube-system
```

> Understood that ALBIngressController resides a lot of drawbacks and left this solution, as it was not able to configure path-routing


##### Google Cloud Platform

> Managed to enable routing specified at the coding challenge

* Created a kubernetes cluster, following the commands below

```
gcloud config set compute/zone us-central1-f
gcloud container clusters create nginx-tutorial --num-nodes=2
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account tiller
```

* Designed the yaml files in order to create the appropriate services, deployments and ingress controller
* Concluded at the `web-app.yaml` file attached, answering on both `/health` and `/ready` endpoints
* A demonstration of the app can be found [here](http://34.120.75.166/)

```
curl -s -o /dev/null -w "%{http_code}" http://34.120.75.166/health --> 200
curl -s -o /dev/null -w "%{http_code}" http://34.120.75.166/ready --> 200
```

> Still trying to find how to connect my postgres instances..


#### Snapshots


![eks cluster](snapshots/aws_eks_cluster.png)

###### *This snapshot depicts the manual-creation of a kubernetes cluster on AWS*


![minikube cluster](snapshots/minikube_cluster.png)

###### *This snapshot depicts the minikube-creation of a kubernetes cluster*


![ekctl cluster](snapshots/eksctl_cluster.png)

###### *This snapshot depicts the eksctl-creation of a kubernetes cluster on AWS*


![gcp cluster](snapshots/google_cluster.png)

###### *This snapshot depicts my final instance's details, run on Google Shell of the GCloud*
